#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float> weights = {1,1,1};
    return weightedIndex(weights);
}

inline Point T(Point P) {
    static int counter = -1;
    counter++;
    switch (counter % 3) {
        case 0: return rotate(P, 2*PI/3)/3;
        case 1: return P;
        case 2: return rotate(P, -2*PI/3)*3;
    }
}

inline Point mycombine(Point P, Point Q) {
    float k = 0.5;
    return P*k + T(Q)*(1-k);
}

int main (void) {
    PointSet X = regularPointSet(3, 500);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(2)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-12.png")
         .draw(Y);
    return 0;
}
