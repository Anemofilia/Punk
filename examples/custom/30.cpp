#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float>
    transitionMatrix[12] = {
         {1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1},
         {1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1},
         {1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1},
         {1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1},
         {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
         {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
         {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
         {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
    };
    return weightedIndex(transitionMatrix[x[0]]);
}

PointSet X = merge(regularPointSet(4, 500),
                   regularPointSet(8, 1000));

inline Point mycombine (Point P, Point Q) {
    float k = 0.24;
    return P * k + Q * (1 - k);
}

int main() {
    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-30.png")
         .draw(Y);
    return 0;
}

