#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float>
    transitionMatrix[6] = {
         {0, 0, 1, 1, 1, 0},
         {0, 0, 0, 1, 1, 1},
         {1, 0, 0, 0, 1, 1},
         {1, 1, 0, 0, 0, 1},
         {1, 1, 1, 0, 0, 0},
         {0, 1, 1, 1, 0, 0}};
    return weightedIndex(transitionMatrix[x[0]]);
}

inline Point mycombine (Point P, Point Q) {
    float k = 0.5; 
    return P*k + Q*(1-k);
}

int main() {
    PointSet X = regularPointSet(6, 1000);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(2)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-0.png")
         .draw(Y);
    return 0;
}
