#include "../../punk.hpp"

inline auto T (Point P) {
    static std::vector<unsigned int> memory = {0};
    unsigned int state;
    memory.emplace_back(state);
    std::vector<unsigned int> temp(memory.begin() + 1, memory.end());
    memory = std::move(temp);

    std::vector<float>
    transitionMatrix[4] = {
        {0, 1, 1, 1},
        {1, 0, 1, 1},
        {1, 1, 0, 1},
        {1, 1, 1, 0}
    };

    unsigned int random = weightedIndex(transitionMatrix[memory[0]]);
    switch (random) {
        case 0:
           return Point( -0.85 * P.x + 0.04 * P.y,
                          0.04 * P.x - 0.85 * P.y - 1.7);
        case 1:
           return Point( 0.35 * P.x + 0.04 * P.y,
                        -0.04 * P.x + 0.35 * P.y - 1.7);
        case 2:
           return Point( 0.85 * P.x - 0.04 * P.y,
                         0.04 * P.x + 0.85 * P.y + 1.7);
        case 3:
           return Point(  0.35 * P.x - 0.04 * P.y,
                         -0.04 * P.x + 0.35 * P.y + 1.7);
    }
}

int main() {
    IFS myIFS;
    auto X = myIFS.setInitialPoint(Point(1000,1000))
                  .setTransformation(T)
                  .simulate();
    X = scale(X, 170);
    X = translate(X,Point(0,-1020));
    Image image;
    image.setPointColor(sf::Color(20,255,20))
         .setFilename("notfern.png")
         .draw(X);
    return 0;
}


