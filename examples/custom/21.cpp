#include <punk.hpp>

template<typename T>
inline std::vector<T>
rotate(unsigned n, std::vector<T> xs) {
    for (int i = 0; i < n; i++) {
        xs.emplace_back(xs[1]);
        std::vector<T> ys(xs.begin() + 1, xs.end());
        xs = ys;
    }
    return xs;
}

inline int myprocess(std::vector<int> x) {
    std::vector<float> xs(1e3, 1);
    return weightedIndex(xs);
}

inline Point mycombine(Point P, Point Q) {
    float k = 0.1;
    return rotate(P*k + Q*(1 - k), 90);
}

inline std::function<Point(double)> f(float k) {
    return [k](double t)
            { return Point(std::pow(cos(t), 3),
                           std::pow(sin(t), 3))*k; };
}

int main (void) {
    PointSet X = sample<double>(5e2, f(1e3), 0, 2*PI);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-21.cpp")
         .draw(Y);
    return 0;
}
