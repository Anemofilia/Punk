#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float>
    transitionMatrix[6][6] = {
        {{1, 0, 1, 1, 1, 0},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1, 1},
         {0, 1, 0, 1, 1, 1},
         {1, 0, 1, 1, 1, 1},
         {1, 0, 1, 1, 1, 1},
         {1, 0, 1, 1, 1, 1},
         {1, 0, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 0, 1, 0, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 0, 1, 0, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 0, 1, 0},
         {1, 1, 1, 1, 1, 1}},

        {{1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {1, 1, 1, 1, 1, 1},
         {0, 1, 1, 1, 0, 1}}
    };
    return weightedIndex(transitionMatrix[x[0]][x[1]]);
}

inline Point mycombine (Point P, Point Q) {
    float k = 0.4; 
    return P*k + Q*(1-k);
}

int main() {
    PointSet X = regularPointSet(6, 1000);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(2)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-1.png")
         .draw(Y);
    return 0;
}
