#include "../../punk.hpp"

inline int myprocess(std::vector<int> x) {
    std::vector<float>
    transitionMatrix[12] = {
        {1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
        {0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0},
        {0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1},
        {1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1},
        {1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1},
        {1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1},
        {1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1},
        {1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1},
        {1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1},
        {1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0},
        {0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0},
        {0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1}
    };
    return weightedIndex(transitionMatrix[x[0]]);
}

inline int sgn(float x) {
    if (x < 0) return -1;
    else return (x > 0);
}

inline float HeronianMean(float x, float y) {
    return std::pow(std::cbrt(x) + std::cbrt(y), 3)/2;
}

inline float mymean(float x, float y) {
    return (x + y)/2;
}

inline float weightedContraharmonicMean(float k, float x, float y) {
    return (pow(k*x,2)+pow((1-k)*y, 2))/(k*x + (1-k)*y);
}

inline Point T(Point P) {
    return rotate(P, PI);
}

inline Point mycombine(Point P, Point Q) {
    float k = 0.3;
    return P*k + Q*(1 - k);
}

int main (void) {
    PointSet X = regularPointSet(12, 1000);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-15.png")
         .draw(Y);
    return 0;
}
