#include <punk.hpp>

template<typename T>
inline std::vector<T> rotate(int n, std::vector<T> slice) {
    for (int i = 0; i < n; i++) {
        slice.emplace_back(slice.front());
        std::vector<T> temp(slice.begin() + 1, slice.end());
        slice = temp;
    }
    return slice;
}

inline int myprocess(std::vector<int> x) {
    std::vector<float> vec;
    for (int i = 0; i < 100; i++) vec.emplace_back(0);
    for (int i = 0; i < 100; i++) vec.emplace_back(1);
    return weightedIndex(rotate<float>(x[0], vec));
}

inline Point mycombine(Point P, Point Q) {
    float k = sierpinskiFactor(200);
    return P*k + Q*(1 - k);
}

inline Point f(double t) {
    double r = 500;
    double cosine = cos(t);
    double sine = sin(t);
    return Point((float) 0.8 + cosine*(1 - cosine),
                 (float) sine*(1 - cosine))*r;
}

int main (void) {
    PointSet X = sample<double>(200, f, 0, 6*PI);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-18.png")
         .draw(Y);
    return 0;
}
