#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float>
    transitionMatrix[8] = {
        {1, 0, 1, 1, 1, 1, 1, 0},
        {0, 1, 0, 1, 1, 1, 1, 1},
        {1, 0, 1, 0, 1, 1, 1, 1},
        {1, 1, 0, 1, 0, 1, 1, 1},
        {1, 1, 1, 0, 1, 0, 1, 1},
        {1, 1, 1, 1, 0, 1, 0, 1},
        {1, 1, 1, 1, 1, 0, 1, 0},
        {0, 1, 1, 1, 1, 1, 0, 1}
    };
    return weightedIndex(transitionMatrix[x[0]]);
}

inline Point mycombine(Point P, Point Q) {
    float k = -sierpinskiFactor(8);
    return P*k + Q*(1-k);
}

int main (void) {
    PointSet X = regularPointSet(8, 500);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-4.png")
         .draw(Y);
    return 0;
}

