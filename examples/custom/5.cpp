#include <punk.hpp>

inline int myprocess(std::vector<int> x) {
    std::vector<float>
    transitionMatrix[8] = {
        {1, 0, 1, 1, 1, 1, 1, 0},
        {0, 1, 0, 1, 1, 1, 1, 1},
        {1, 0, 1, 0, 1, 1, 1, 1},
        {1, 1, 0, 1, 0, 1, 1, 1},
        {1, 1, 1, 0, 1, 0, 1, 1},
        {1, 1, 1, 1, 0, 1, 0, 1},
        {1, 1, 1, 1, 1, 0, 1, 0},
        {0, 1, 1, 1, 1, 1, 0, 1}
    };
    return weightedIndex(transitionMatrix[x[0]]);
}

inline Point T(const Point &P) {
    return rotate(P, PI/2);
}

inline Point mycombine(const Point &P, const Point &Q) {
    float k = 0.3;
    return T(P*k + Q*(1-k));
}

int main() {
    PointSet X = regularPointSet(8, 1000);

    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)
                   .setReferencePointSet(X)
                   .setProcessOrder(1)
                   .setProcess(myprocess)
                   .setCombinationFunction(mycombine)
                   .simulate();

    Image image;
    image.setPointColorFunction(angularRainbow)
         .setFilename("Punk-5.png")
         .draw(Y);
    return 0;
}
