#include "../../punk.hpp"

/* myprocess(n) é um processo estocástico que retorna, com probabilidade
uniforme, um número de 0 a n - 1. Usamos esses números como identificadores
dos pontos de referência (tipicamente numeramos no sentido anti-horário) */
auto myprocess(int n) {
    return [n](std::vector<int> x) {
            std::uniform_int_distribution<> dis(0, n - 1);
            return dis(gen);
    };
}

/* mycombine(n) é uma função que recebe dois pontos P e Q e retorna um ponto
intermediário a eles. O coeficiente que determina quão próximo de P, nesse
exemplo é definida por uma função na biblioteca punk.hpp, chamada sierpinskiFactor */
auto mycombine (int n) {
    return [n](const Point &P, const Point &Q) {
        float k = sierpinskiFactor(n);
        return P*k + Q*(1-k);
    };
}

int main(int argc, char* argv[]) {
    Image image;

    // Guarda o valor do argumento passado pro programa como inteiro
    int n = std::atoi(argv[1]);

    // Cria um conjunto de n pontos, dispostos na forma de eneágono regular
    PointSet X = regularPointSet(n, 1000);

    /* Simula 10^6 pontos, a partir do conjunto de pontos de referência X
       O número 0 corresponde a ordem do processo estocástico, isso é, quantos
       estados anteriores ele leva em consideração para gerar o próximo estado.
       A função myprocess(n) sorteia os vértices aleatórios e,
       A função mycombine(n) combina o vértice aleatório com a última iteração,
       gerando um novo ponto.
    */
    ChaosGame CG;
    PointSet Y = CG.setIterations(1e6)                   // Define o número de iterações
                   .setReferencePointSet(X)              // Define o conjunto dos pontos de referência
                   .setProcess(myprocess(n))             // Define o processo estocástico que sorteia os vértices
                   .setCombinationFunction(mycombine(n)) // Define a regra de combinar os vértices
                   .simulate();                          // Executa a simulação, produzindo um novo PointSet

    // Gera uma imagem a partir do conjunto de pontos
    image.setPointColorFunction(angularRainbow) // Define uma regra pra colorir os pontos
         .setWidth(2200)                        // Largura da imagem
         .setHeight(2200)                       // Altura da imagem
         .setFilename("Sierpinski "+ std::to_string(n) +"-gon.jpg") // Nome da imagem
         .draw(Y);
    return 0;
}

