(use-modules (guix profiles)
             (guix packages)
             (guix gexp)
             (guix git-download)
             (guix build-system copy)
             (gnu packages base)
             (gnu packages commencement)
             (gnu packages game-development)
             (gnu packages llvm)
             ((guix licenses) #:prefix license:))

(define-public punk
  (let ((commit "a7414d9968100e0fb25b29bc953ac4e00e52ea9b")
        (revision "0"))
    (package
     (name "punk")
     (version (git-version "1.0.0" revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://codeberg.org/anemofilia/punk")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32 "1yvgmn3j7l8rsa7g39hc26f1w4kvd46qkaiibiywmr93i7k7d0f7"))))
     (build-system copy-build-system)
     (arguments (list #:install-plan #~'(("src/" "include"))))
     (propagated-inputs (list sfml libomp))
     (home-page "https://codeberg.org/anemofilia/punk")
     (synopsis "Simple and fast fractal generator.")
     (description "Punk provides a basic toolchain for building fractals through
complex systems like Chaos Game and the Iterated Function System.")
     (license license:gpl3+))))

(packages->manifest
  (list gnu-make
        gcc-toolchain
        punk))
