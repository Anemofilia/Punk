#include "pointSet.hpp"
#include "process.hpp"
#include "utils.hpp"
#include <optional>
#include <chrono>
#include <functional>
#include <iostream>

/* Returns a PointSet Y corresponding to the result of n iterations
   of the Chaos Game, using a stochastic process of order N and
   referencePointSet.size() states and a function combine to yield
   new points. */
[[nodiscard]]
PointSet simulateChaosGame(int n,
                           PointSet referencePointSet,
                           int N, // order of the stochastic process
                           std::function<int(std::vector<int>)> process,
                           std::function<Point(const Point&, const Point&)> combine,
                           Point initialPoint) {

    std::cout << "Simulating points..." << std::endl;
    auto start = std::chrono::high_resolution_clock::now();

    std::vector<Point> points(n);
    if (N == 0) N++;
    std::vector<int> processSlice(N);
    std::uniform_int_distribution<> dis(0, referencePointSet.size() - 1);
    for (int i = 0; i < N; i++) processSlice[i] = dis(gen);

    /* Map the numbers to the corresponding points in referencePoint
       storing it in combinationSlice */
    int num = processSlice.back();
    Point referencePoint = referencePointSet[num];

    Point iterationPoint = initialPoint;
    points.emplace_back(iterationPoint);

    for (int i = 0; i < n; ++i) {
        // update processSlice with a new element
        num = process(processSlice);
        std::vector<int> temp(processSlice.begin() + 1, processSlice.end());
        processSlice = std::move(temp);
        processSlice.emplace_back(num);

        // update referencePoint
        referencePoint = referencePointSet[num];

        // update iterationPoint
        iterationPoint = combine(iterationPoint, referencePoint);

        // Store it on simulate output
        points.emplace_back(iterationPoint);
    }
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "    "
              << "Elapsed simulation time: "
              << (double) std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count()/1000
              << "s" << std::endl;
    return PointSet(points);
}

class ChaosGame {
public:
    ChaosGame() {}

    ChaosGame& setIterations(unsigned int iterations) {
        m_iterations = iterations;
        return *this;
    }

    ChaosGame& setReferencePointSet(PointSet referencePointSet) {
        m_referencePointSet = referencePointSet;
        return *this;
    }

    ChaosGame& setProcessOrder(unsigned int processOrder) {
        m_processOrder = processOrder;
        return *this;
    }

    ChaosGame& setProcess(std::function<int(std::vector<int>)> process) {
        m_process = process;
        return *this;
    }

    typedef std::function<Point(const Point&, const Point&)> T;
    ChaosGame& setCombinationFunction(T combinationFunction) {
        m_combinationFunction = combinationFunction;
        return *this;
    }

    ChaosGame& setInitialPoint(Point initialPoint) {
        m_initialPoint = initialPoint;
        return *this;
    }

    PointSet simulate() {
        return ::simulateChaosGame(m_iterations,
                                   m_referencePointSet,
                                   m_processOrder,
                                   m_process,
                                   m_combinationFunction,
                                   m_initialPoint);
    }

private:
    unsigned int m_iterations = 1e6;
    PointSet m_referencePointSet;
    unsigned int m_processOrder;
    std::function<int(std::vector<int>)> m_process;
    std::function<Point(const Point&, const Point&)> m_combinationFunction;
    Point m_initialPoint = randomPoint(500,500);
};
