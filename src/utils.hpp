#pragma once
#include <cmath>
#include <random>
#define PI 3.14159265358979323846264338327950

std::random_device rd;
std::mt19937 gen(rd());


/* Returns a float corresponding to the scale factor of a n-sided polygon. */
[[nodiscard]]
inline float sierpinskiFactor(int n) {
    float x = 1;
    for (int k = 1; k <= std::floor(n/4); k++) x += cos(2*PI*k/n);
    return 1/(2*x);
}
