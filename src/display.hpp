#include <chrono>
#include <optional>
#include <SFML/Graphics.hpp>
#include "pointSet.hpp"

/* Returns a std::function<sf::Color(Point)> h that returns a rainbow color
   based on the value of f(p). */
[[nodiscard]]
auto rainbow(const std::function<float(Point)>& f) {
    return [f, theta = 2 * PI / 3.f](Point p) {
        auto g = [v = f(p)](float offset) { return 128 + 127 * sin(v + offset); };
        return sf::Color(g(0), g(theta), g(2 * theta));
    };
}

// Returns a sf::Color corresponding to the argument of the given point
auto angularRainbow = rainbow(theta);

/* Returns a std::function<sf::Color(Point)> h that returns a rainbow color
   based on the value of the modulus of the given point divided by 180*radius. */
auto radialRainbow(int radius) {
    return rainbow([radius](Point p) { return rho(p)/(180*radius); });
}

/* Returns a std::function<sf::Color(Point)> that returns a color, from
   yellow to purple, based on the local density of points. */ 
auto heatmap(PointSet points,
             std::function<float(Point, Point)>
             metric = [](Point p, Point q) { return rho(p - q); }) {
    return [points, metric](Point q) {
        const float radius = 20.f;
        const float maxDensity = 20.f;

        std::vector<Point> sampledPoints;
        sampledPoints.reserve(1e3);
        std::uniform_int_distribution<> dis(0, points.size());
        for (int i = 0; i < 1e3; i++)
            sampledPoints.emplace_back(points[dis(gen)]);

        float density = 0.f;
        #pragma omp paralell for reduction(+:density)
        for (Point& p : sampledPoints)
            density += std::max(1.f - metric(p, q) / radius, 0.f);

        density = std::min(density, maxDensity) / maxDensity;
        sf::Uint8 t = density * 255.f;
        return sf::Color(255, 1 - t, t);
    };
}


/* Creates and saves, in the current dir, a image of the PointSet X
   with the specified width, height, backgroundColor and filename.
   Each point is colored with the value of pointColorFunction applied
   to it. In the case that no pointColorFunction is supplied, but a
   pointColor is supplied, every point is colored with it. If neither
   a pointColor nor a pointColorFunction are supplied, then each point
   is colored by default with the color #A790D3, or, in RGB:
   (167, 144, 211). */
void draw(PointSet X,
          unsigned int width = 2200,
          unsigned int height = 2000,
          sf::Color backgroundColor = sf::Color::Black,
          sf::Color pointColor = sf::Color(167, 144, 211),
          std::optional<std::function<sf::Color(Point)>>
          pointColorFunction = std::nullopt,
          std::string filename = "Punk.png") {

    std::cout << "Rendering image..." << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    sf::Image image;
    image.create(width, height, backgroundColor);

    auto f = (pointColorFunction == std::nullopt) ?
             [pointColor](Point _) { return pointColor; } :
             pointColorFunction.value();

    // Draw points to image
    #pragma omp parallel for shared(image)
    for (Point& p : X) {
        int x = 0.5*width - p.x;
        int y = 0.5*height - p.y;
        if (x >= 0 && x < width && y >= 0 && y < height)
            image.setPixel(x, y, f(p));
    }

    // Display elapsed render time
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "    "
              << "Elapsed render time: "
              << (double) std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count()/1000
              << "s" << std::endl;

    // Save the image to a file
    if (image.saveToFile(filename)) {
        std::cout << "Image saved to " << filename << std::endl;
    } else {
        std::cout << "Failed to save image" << std::endl;
    }
}

// Auxiliar class for the function draw
class Image {
public:
    Image() {}

    Image& setWidth(unsigned int width) {
        m_width = width;
        return *this;
    }

    Image& setHeight(unsigned int height) {
        m_height = height;
        return *this;
    }

    Image& setBackgroundColor(sf::Color backgroundColor) {
        m_backgroundColor = backgroundColor;
        return *this;
    }

    Image& setPointColor(sf::Color pointColor) {
        m_pointColor = pointColor;
        return *this;
    }

    Image& setPointColorFunction(std::function<sf::Color(Point)> pointColorFunction) {
        m_pointColorFunction = pointColorFunction;
        return *this;
    }

    Image& setFilename(std::string filename) {
        m_filename = filename;
        return *this;
    }

    void draw(PointSet& X) {
        ::draw(X,
               m_width,
               m_height,
               m_backgroundColor,
               m_pointColor,
               m_pointColorFunction,
               m_filename);
    }

private:
    unsigned int m_width = 2200;
    unsigned int m_height = 2000;
    sf::Color m_backgroundColor = sf::Color::Black;
    sf::Color m_pointColor = sf::Color(167, 144, 211);
    std::optional<std::function<sf::Color(Point)>>
    m_pointColorFunction = std::nullopt;
    std::string m_filename = "Punk.png";
};

/* Creates and saves, in the current dir, a video of the PointSet X
   with the specified width, height, fps, pointsPerFrame, backgroundColor
   and filename.
   Each point is colored with the value of pointColorFunction applied
   to it. In the case that no pointColorFunction is supplied, but a
   pointColor is supplied, every point is colored with it.
   If neither a pointColor nor a pointColorFunction are supplied, then
   each point is colored by default with the color #A790D3, or, in RGB:
   (167, 144, 211).
   Currently in progress. */
//void animate(PointSet X,
//             unsigned width = 2200,
//             unsigned height = 2000,
//             unsigned fps = 60,
//             sf::Color backgroundColor = sf::Color::Black,
//             sf::Color pointColor = sf::Color(167, 144, 211),
//             std::optional<std::function<sf::Color(Point)>> pointColorFunction = std::nullopt,
//             std::string filename = "Punk.png") {
//
//    std::cout << "Rendering video..." << std::endl;
//    auto start = std::chrono::high_resolution_clock::now();
//    sf::Image frame;
//    frame.create(width, height, backgroundColor);
//
//    auto f = (pointColorFunction == std::nullopt) ?
//             [pointColor](Point _) { return pointColor; } :
//             pointColorFunction.value();
//
//    for (Point& p : X) {
//        int x = 0.5*width - p.x;
//        int y = 0.5*height - p.y;
//        // Add p to the old frame
//        if (x >= 0 && x < width && y >= 0 && y < height)
//            frame.setPixel(x, y, f(p));
//        // Add the modified frame to the video
//        /* Something here */
//    }
//
//    // Display elapsed render time
//    auto stop = std::chrono::high_resolution_clock::now();
//    std::cout << "    "
//              << "Elapsed render time: "
//              << (double) std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count()/1000
//              << "s" << std::endl;
//
//    // Save the video to a file
//    if (video.saveToFile(filename)) {
//        std::cout << "Image saved to " << filename << std::endl;
//    } else {
//        std::cout << "Failed to save video" << std::endl;
//    }
//}
//
//// Auxiliar class for the function animate
//class Video {
//public:
//    Video() {}
//
//    Video& setWidth(int width) {
//        m_width = width;
//        return *this;
//    }
//
//    Video& setHeight(int height) {
//        m_height = height;
//        return *this;
//    }
//
//    Video& setFPS(int fps) {
//        m_fps = fps;
//        return *this;
//    }
//
//    Video& setPointsPerFrame(int pointsPerFrame) {
//        m_pointsPerFrame = pointsPerFrame;
//        return *this;
//    }
//
//    Video& setSaveFinalState(bool value) {
//        m_saveFinalState = value;
//        return *this;
//    }
//
//    Video& setBackgroundColor(sf::Color backgroundColor) {
//        m_backgroundColor = backgroundColor;
//        return *this;
//    }
//
//    Video& setPointColor(sf::Color pointColor) {
//        m_pointColor = pointColor;
//        return *this;
//    }
//
//    Video& setPointColorFunction(std::function<sf::Color(Point)> pointColorFunction) {
//        m_pointColorFunction = pointColorFunction;
//        return *this;
//    }
//
//    Video& setFilename(std::string filename) {
//        m_filename = filename;
//        return *this;
//    }
//
//    Video& setFfmpegPath(std::string path) {
//        m_ffmpegPath = path;
//        return *this;
//    }
//
//    // void animate(PointSet& X) {
//    //     ::animate(X,
//    //            m_width,
//    //            m_height,
//    //            m_backgroundColor,
//    //            m_pointColor,
//    //            m_pointColorFunction);
//    //            m_filename,
//    //            m_ffmpegPath);
//    // }
//private:
//    int m_width = 2200;
//    int m_height = 2000;
//    int m_fps = 60;
//    int m_pointsPerFrame = 10;
//    bool m_saveFinalState = false;
//    sf::Color m_backgroundColor = sf::Color::Black;
//    sf::Color m_pointColor = sf::Color(167, 144, 211);
//    std::optional<std::function<sf::Color(Point)>>
//    m_pointColorFunction = std::nullopt;
//    std::string m_filename = "Punk.mp4";
//    std::string m_ffmpegPath = "ffmpeg";
//};
